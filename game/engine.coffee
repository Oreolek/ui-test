###
# This is the file with all thing *around* Salet.
###

salet.game_id = "534fed17-ca70-483c-994a-002a332e5928"
salet.game_version = "1.0"
salet.optionsRoom = "settings"
salet.start = "language"

switchTab = (tabid) ->
  $(".tab").removeClass("active")
  $("#"+tabid).addClass("active")
  if tabid == "story" and not salet.here().canSave
    salet.goBack()

$(document).ready(() ->
  window.addEventListener('popstate', (event) ->
    salet.goBack()
  )
  $("body").on("click", ".tab", (event) ->
    id = $(event.target).attr("id")
    if not id?
      id = $(event.target).parent().attr("id")
    switchTab(id)
    return true
  )
  $("body").on("click", '#night', () ->
    if (window.night)
      $("body").removeClass("night")
      $("#night").removeClass("active")
      window.night = false
    else
      $("body").addClass("night")
      $("#night").addClass("active")
      window.night = true
  )
  $("#page").on("click", "a", (event) ->
    if (window.hasOwnProperty('TogetherJS') and !window.remote and TogetherJS.running)
      options = {
        type: "click"
      }
      link = $(event.target)
      if link.attr("id") != undefined
        options.id = link.attr("id")
      if link.attr("href") != undefined
        options.href = link.attr("href")
      if options.href == undefined and options.id == undefined
        return
      TogetherJS.send(options)
  )
  window.remote = false
  if (window.hasOwnProperty('TogetherJS'))
    TogetherJS.config("ignoreForms", true)
    TogetherJS.config("ignoreMessages", [
      "cursor-update"
      "keydown"
      "scroll-update"
      "form-focus"
      "cursor-click"
    ])
    TogetherJS.hub.on("click", (msg) ->
      if (! msg.sameUrl)
        return
      window.remote = true
      if msg.id != undefined
        $("##{msg.id}").trigger("click")
      else if msg.href != undefined
        $("#page a[href=#{msg.href}]").trigger("click")
      window.remote = false
    )
  salet.beginGame()
)
