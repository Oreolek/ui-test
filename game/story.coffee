###
# This is the story code.
###

croom "start",
  tags: ["menu"]
  title: () -> "begingame".l()
  choices: "#choice"

croom "choice1",
  tags: ["choice"]
  choices: "#choice"
  title: "Поздороваться с Облонским"
  subtitle: "Необходимо нивелировать быличку между нами"

croom "choice2",
  tags: ["choice"]
  choices: "#choice"
  title: "Отложить сетературу"
  subtitle: "Окунуться в безбрежный океан бессюжетности"

croom "choice3",
  tags: ["choice"]
  choices: "#choice"
  title: "Анна Каренина"
  subtitle: "Это прямая цитата из романа, между прочим"
