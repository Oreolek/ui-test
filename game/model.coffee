###
# This is the file with all logic code *inside* Salet.
# This is not story code but something around the story and inside the game.
###

###
Element helpers. There is no real need to build monsters like a().id("hello")
because you won't use them as is. It does not make sense in context, the
author has Markdown and all utilities to *forget* about the markup.
###
way_to = (content, ref) ->
  return "<a href='#{ref}' class='way'>#{content}</a>"
textlink = (content, ref) ->
  return "<a href='./_writer_#{ref}' class='once'>#{content}</a>"
actlink = (content, ref) ->
 return "<a href='./#{ref}' class='once'>#{content}</a>"
choice = (text, url) ->
  retval = '<a'
  if url?
    retval += " href='#{url}'"
  retval += "><div><div class='title'>#{text}</div></div></a>"
  return retval

sysroom = (name, options) ->
  options.canSave = false
  options.exit = () ->
    if document.querySelector('#current-room')
      salet.view.clearContent('#current-room')
    $(".action").show()
  options.dsc ?= () ->
    return @text.fcall()+"\n\n"+"""
    <div class="center"><a href="./exit" tabindex=999><button class="btn btn-lg btn-outline-primary">#{"back".l()}</button></a></div>
    """
  options.text ?= () -> name.l()
  options.actions = {
    exit: () ->
      return salet.goBack()
  }
  return croom(name, options)

croom = (name, spec) ->
  spec.clear ?= true
  spec.optionColor ?= ""
  spec.optionText ?= () ->
    retval = """
      <div class="#{spec.optionColor}">
        <div class="title">#{spec.title.fcall()}</div>
    """
    if (spec.subtitle?)
      retval += """
        <div class="subtitle">#{spec.subtitle.fcall()}</div>
      """
    retval += '</div>'
  spec.dsc ?= () -> name.l()
  return room(name, spec)

$(document).on("room_enter", (event, data) ->
  # Piwik analytics: room stats
  if salet.interactive and _paq?
    _paq.push(['trackPageView', data.to])
)

sysroom "inventory",
  text: () ->
    if salet.character.inventory.length == 0
      text = "inventory_empty".l()
    else
      text = "inventory_contains".l()+"\n\n"
      for thing in salet.character.inventory
        text += "* #{salet.character.listinv(thing.name)}\n"

sysroom "map",
  text: () ->
    return "<div id='map'></div>"
  after: () ->
    data = {
      edges: []
      nodes: []
    }
    edges = []
    rooms = []
    globx = 1
    globy = 1
    deltas = [
      # [1, 0], # looks bad on our map
      [0, 1],
      [-1, 0],
      [0, -1],
    ]
    for name, room of salet.rooms
      if room.canSave == false or name == "start"
        continue
      if rooms.indexOf(name) == -1
        data.nodes.push({
          "id": name
          "label": room.title()
          "size": 5
          "color": "#000"
          "x": globx
          "y": globy
        })
        rooms.push(name)
      if room.ways? and room.ways.length > 0
        delta = 0
        for way in room.ways
          id = "edge_"+name+"_"+way
          # we don't want to display a two-way link twice
          if edges.indexOf("edge_"+way+"_"+name) == -1
            edges.push(id)
            data.edges.push({
              "id": id
              "source": room.name
              "target": way
              "size": 1
              "color": "#ccc"
            })
          if rooms.indexOf(way) == -1
            data.nodes.push({
              "id": way
              "label": salet.rooms[way].title()
              "size": 5
              "color": "#000"
              "x": globx + deltas[delta][0]
              "y": globy + deltas[delta][1]
            })
            rooms.push(way)
            delta++
        globy = globy - 2
    s = new sigma({
      graph: data,
      container: 'map'
    })
    s.bind('clickNode', (e) ->
      switchTab("storytab")
      salet.goTo(e.data.node.id)
    )
    return ""

$(document).on("room_language_after_choices", () ->
  $(".options").addClass("narrowchoice")
)
sysroom "language",
  dsc: ""
  choices: "#language"

sysroom "ru",
  title: "Русский",
  tags: ["language"]
  dsc: ""
  enter: () ->
    i18n.lang = "ru"
    salet.goTo('menu')

sysroom "en",
  title: "English",
  tags: ["language"]
  canChoose: false
  dsc: ""
  enter: () ->
    i18n.lang = "en"
    salet.goTo('menu')

sysroom "menu",
  dsc: ""
  choices: "#menu"

sysroom "settings",
  tags: ["menu"]
  title: () -> "settings_title".l()
  text: () ->
    nightclass = ""
    if window.night
      nightclass = "active"
    return "credits".l() + """\n
      <ul class="options">
        <li id="night" tabindex=1 class="#{nightclass}">#{choice("night".l())}</li>
        <li tabindex=2 onclick="TogetherJS(this); return false;">
          #{choice("multiplayer".l())}
        </li>
      </ul>
    """

sysroom "inventory",
  text: () ->
    if salet.character.inventory.length == 0
      text = "inventory_empty".l()
    else
      text = "inventory_contains".l()+"\n\n"
      for thing in salet.character.inventory
        text += "* #{salet.character.listinv(thing.name)}\n"

sysroom "map",
  text: () -> """
    Здесь будет карта
  """
