salet.autosave = false;
salet.autoload = false;

$(document).ready(function() {
  QUnit.test("The game starts okay.", function(assert) {
    assert.notEqual(salet, void 0, "Salet is initialized");
    return assert.equal(salet.current, "start", "Salet is in the room called 'start'");
  });
  QUnit.test("There are no game-breaking bugs when entering rooms.", function(assert) {
    for (var key in salet.rooms) {
      // skip loop if the property is from prototype
      if (!salet.rooms.hasOwnProperty(key)) continue;

      var room = salet.rooms[key];

      assert.ok(salet.goTo(room.name), "Entered room "+room.name);
    }
  });
  QUnit.test("There are no game-breaking bugs in all actions.", function(assert) {
    for (var key in salet.rooms) {
      // skip loop if the property is from prototype
      if (!salet.rooms.hasOwnProperty(key)) continue;
      var room = salet.rooms[key];

      salet.goTo(room.name);
      for (var act in room.actions) {
        if (!room.actions.hasOwnProperty(act)) continue;
        assert.ok(act.fcall(room), "Executed action "+act);
      }
      for (var act in room.writers) {
        if (!room.writers.hasOwnProperty(act)) continue;
        assert.ok(act.fcall(room), "Executed action "+act);
      }
    }
  });
});
